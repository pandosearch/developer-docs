FROM python:alpine
RUN apk add --no-cache --virtual .build gcc musl-dev \
 && pip install mkdocs pygments \
 && apk del .build

WORKDIR /app

EXPOSE 8000
CMD ["mkdocs", "serve", "--dev-addr", "0.0.0.0:8000"]
