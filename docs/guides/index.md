# Introduction

Pandosearch enables you to show search query suggestions, search results and recommendations to your visitors based on the content available on your website. We make sure all data is kept up-to-date by periodically revisiting your website. Our API provides all of this data to you.

While the above saves you a lot of work, Pandosearch cannot provide everything. Two things in particular are up to you to arrange:

* The search interface for your website visitors
* If needed: content adjustments to improve the search experience for visitors

These guides aim to help you with those tasks by offering practical information on how to do this.

## Search interface

We have the following guides to help you on your way with building a search interface:

* [Live suggestions](live-suggestions.md) – give instant feedback to visitors while typing in a search query
* [Search results](search-results.md) – retrieve search results for a given search query and display them on a web page

## Content adjustments

We have a couple of content adjustment guides available:

* [Keymatch](keymatch.md) – how to mark content as the top result for a given search term
* [Sitemaps](sitemaps.md) – how to allow Pandosearch to discover and use Sitemaps XML data

Which content adjustments are relevant for you depends on your specific situation and configuration. In case of doubt, just reach out to us using one of the support channels available for your plan.
