# Developer documentation

Hi! Welcome to the Pandosearch developer documentation.

These pages contain technical guides and reference documentation for working with Pandosearch.

Intended readers are people implementing Pandosearch, such as software developers and content management system experts.

The documentation consists of the following sections:

- [Guides](guides/index.md) – Practical guidance on how to build search user interfaces backed by Pandosearch data, and articles about content preparation and optimization.
- [API Reference](api/index.md) – Technical reference documentation for the Pandosearch API.

Content is updated regularly based on user feedback and newly added features. The source repository is [available online](https://gitlab.com/pandosearch/developer-docs) in case you want to track changes. Your feedback is welcome!
